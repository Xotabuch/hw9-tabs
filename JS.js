let tabs = document.querySelector(".tabs");
let tabsContent = document.querySelector(".tabs-content");

tabs.addEventListener("click", (e) => {
  let position = 0;
  for (let i = 0; i < tabs.children.length; i++) {
    tabs.children[i].classList.remove("active");
    if (tabs.children[i] === e.target) {
      position = i;
      tabs.children[i].classList.add("active");
    }
  }
  for (let i = 0; i < tabsContent.children.length; i++) {
    tabsContent.children[i].hidden = true;
    if (i === position) {
      tabsContent.children[i].hidden = false;
    }
  }
});
